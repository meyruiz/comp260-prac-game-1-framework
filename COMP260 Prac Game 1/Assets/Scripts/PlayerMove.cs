﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 10.0f; // in metres/second/second
	public float brake = 100.0f; // in metres/second/second
	public float turnSpeed = 30.0f; // in degrees/second
	public string axisX;
	public string axisY;

	private float speed = 0.0f;    // in metres/second
	private BeeSpawner beeSpawner;
	public float destroyRadius = 1.0f; 

	void Start() {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}
		
	void Update() {
		if (Input.GetButtonDown ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

		// the horizontal axis controls the turn
		float turn = Input.GetAxis(axisX);
		float turnMovement = turn *turnSpeed * speed * Time.deltaTime * -1;

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis(axisY);

		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
			//turnMovement *= -1;
		} else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		} else {
			// braking
			if (speed > 0 && (speed - brake * Time.deltaTime) > 0) {
				speed = speed - brake * Time.deltaTime;
				//turnMovement *= -1;
			} else if (speed < 0 && (speed + brake * Time.deltaTime) < 0) {
				speed = speed + brake * Time.deltaTime;
			} else {
				speed = 0;
			}
		}

		// turn the car
		transform.Rotate(0, 0, turnMovement);
			
		// clamp the speed
		// tests if the first parameter is within the specified limits
		// if it is too big or too small, it is clamped to the 
		// maximum or minimum respectively
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		// Vector2.up is unit-length vector pointing in the up (Y) direction
		// i.e. the vector (0,1).
		// Vector2 velocity = new Vector2(0, speed);
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}
