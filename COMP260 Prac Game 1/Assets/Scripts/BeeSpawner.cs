﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

	public int nBees = 50;
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;
	private int iBeeNum = 0;
	public float minBeePeriod, maxBeePeriod;
	//public float timestamp = 0.0f;
	private float time;
	bool executing = false;

	void Start() {
		// time = minBeePeriod + Random.value * maxBeePeriod;
		// InvokeRepeating ("CreateBee", 2.0f, time);
	}

	void Update() {
		if (executing == false) {
			StartCoroutine ("CreateBee");
		}
	}

	IEnumerator CreateBee() {

		executing = true;

		// instantiate a bee
		BeeMove bee = Instantiate(beePrefab);
		// attach to this object in the hierarchy
		bee.transform.parent = transform;            
		// give the bee a name and number
		bee.gameObject.name = "Bee " + iBeeNum;
		iBeeNum += 1;

		// move the bee to a random position within 
		// the bounding rectangle
		float x = xMin + Random.value * width;
		float y = yMin + Random.value * height;
		bee.transform.position = new Vector2(x,y);

		time = minBeePeriod + Random.value * maxBeePeriod;
		Debug.Log ("time" + time);
		yield return new WaitForSeconds(time);

		executing = false;
	}

	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			// Fixed bug by adding a type conversion
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}
}
