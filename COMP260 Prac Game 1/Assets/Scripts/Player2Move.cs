﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Move : MonoBehaviour {

	public float maxSpeed = 5.0f;
	public string axisX;
	public string axisY;

	void Update() {
		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis(axisX);
		direction.y = Input.GetAxis(axisY);

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}
}